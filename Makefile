.PHONY: clean compile treebank

clean:
	rm -f src/*.{gfo,pgf} build

compile:
	mkdir -p build
	gf -make --gfo-dir=build -optimize-pgf src/AAC.gf src/AAC*.gf
	rm AAC.gfo
	mv AAC.pgf build/

treebank:
	gf --run <tests/treebank.gfs
