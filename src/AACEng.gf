concrete AACEng of AAC = {

    lincat
        -- have attribute s which is Str
        BasePhrase, Phrase, Item, Kind, Activity, Quality = {s : Str} ;

    lin
        PolitePhrase phrase = {s = phrase.s ++ "please"} ;

        DesirePhrase phrase = {s = "I want" ++ phrase.s } ;
        NeedPhrase phrase = {s = "I need" ++ phrase.s } ;
        NeutralPhrase phrase = phrase ;

        -- Is item quality = {s = item.s ++ "is" ++ quality.s} ;
        -- Question item quality = {s = "is" ++ item.s ++ quality.s } ;

        WordPhrase_2 k k2 = { s = k.s ++ "and" ++ k2.s } ;
        WordPhrase_3 k k2 k3 = {
            s = k.s ++ k2.s ++ "and" ++ k3.s
        } ;
        WordPhrase_4 k k2 k3 k4 = {
            s = k.s ++ k2.s ++ k3.s ++ "and" ++ k4.s
        } ;
        KindPhrase k = k ;

        -- i suspect these can be replaced with DetSg (determiner singular)
        This kind = {s = "this" ++ kind.s} ;
        That kind = {s = "that" ++ kind.s} ;

        QKind quality kind = {s = quality.s ++ kind.s} ;

        -- string literal linearizations
        -- Symbols
        iPad = {s = "ipad" } ;
        Snack = {s = "snack" } ;
        Juice = {s = "juice" } ;
        Soother = {s = "soother"} ;
        Potty = {s = "potty" | "toilet" | "bathroom" } ;
        Play = {s = "play" } ;
        -- Help = {s = "help" }

        -- Introduction phrases

        -- intensifier
        Very quality = {s = "very" ++ quality.s} ;

}
