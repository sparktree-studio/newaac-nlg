concrete AACNVS of AAC = open SyntaxNVS, ToneNVS in {
    lincat

        BasePhrase, Phrase, Item, Kind, Activity, Quality = { s : Str} ;

    lin
        PolitePhrase phrase = { s = PoliteTag.s ++ phrase.s } ;
        DesirePhrase phrase = {s = "[want]" ++ phrase.s} ;
        NeedPhrase phrase = {s = "[need]" ++ phrase.s} ;
        NeutralPhrase phrase = phrase ;

        WordPhrase_2 kind kind2 = { s = kind.s ++ kind2.s } ;
        WordPhrase_3 kind kind2 kind3 = {
            s = kind.s ++ kind2.s ++ kind3.s
        } ;
        WordPhrase_4 kind kind2 kind3 kind4 = {
            s = kind.s ++ kind2.s ++ kind3.s ++ kind4.s
        } ;
        KindPhrase kind = kind ;

        -- Is item quality = { s = item.s ++ IsInd.s ++ quality.s} ;
        -- Question item quality = { s = QuesInd.s ++ wrapNp (item.s ++ quality.s)} ;

        -- NVS: prefixed ! means: this particular one (sg definite determiner)
        This kind = { s = ThisInd.s ++ kind.s } ;
        -- NVS: prefixed > means: that particular one (sg definite determiner)
        That kind = { s = ThatInd.s ++ kind.s } ;

        QKind quality kind = { s = wrapNp(kind.s ++ quality.s) };

        iPad = { s = "tablet.n.06" } ;
        Snack = { s = "snack.n.01" } ;
        Juice = { s = "drink.n.01" } ;
        Soother = { s = "pacifier.n.02" } ;
        Potty = { s = "toilet.v.01" } ; -- toilet
        Play = { s = "play.v.01" } ; -- play

        QuesWhere = { s = "where.i.01" } ;
        QuesWhen = { s = "when.i.01" } ;
        -- Help = { s = "help" }

        -- intensifier
        Very quality = { s = quality.s ++ IntensityInd.s} ;
}
