abstract AAC = {
    flags startcat = Phrase ;

    cat
        BasePhrase ; Phrase ; -- main collections of Items
        Kind ; -- visual representation of a word/concept
        Item ; -- Symbol plus modifiers or qualities
        Activity; -- verb or action
        Quality ; -- adjective or adverb

    fun
        PolitePhrase : BasePhrase -> Phrase ;

        DesirePhrase : BasePhrase -> Phrase ;
        NeedPhrase : BasePhrase -> Phrase ;
        NeutralPhrase : BasePhrase -> Phrase ;

        -- Is : Item -> Quality -> BasePhrase ; -- item is quality
        -- Question: Item -> Quality -> BasePhrase ;


        WordPhrase_2 : Kind -> Kind -> BasePhrase ;
        WordPhrase_3 : Kind -> Kind -> Kind -> BasePhrase ;
        WordPhrase_4 : Kind -> Kind -> Kind -> Kind -> BasePhrase ;
        KindPhrase : Kind -> Phrase ;

        This, That : Kind -> Item ; -- this spaghetti

        QKind : Quality -> Kind -> Kind ; -- good cheese

        iPad, Snack, Juice, Soother, Potty, Play : Kind ;

        -- Help : Activity ;

        -- intensifier
        Very : Quality -> Quality ;
}
