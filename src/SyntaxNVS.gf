resource SyntaxNVS = {

    oper
        wrapNp : Str -> Str ;
        wrapNp str = OpenItem.s ++ str ++ CloseItem.s ;

        -- Syntax
        OpenItem : {s : Str} = {s = "{"};
        CloseItem : {s : Str} = {s = "}"};
        IsInd : {s : Str} = {s = "->"} ;
        QuesInd : {s : Str} = {s = "?"} ;
        ThisInd : {s : Str} = {s = "!"} ;
        ThatInd : {s : Str} = {s = ">"} ;
        IntensityInd : {s : Str} = {s = "^"} ;
}
